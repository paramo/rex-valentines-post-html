(function ($, root, undefined) {
	$(function () {
		'use strict';

        var colors = ['#EBB3C3', '#57C0E8', '#74BF99', '#FEDD3C', '#9D7CD3'];
        $(".link").on("mouseenter", function(){
            $(this).css("background-color", colors[Math.floor(Math.random() * colors.length) | 0])
        });
        $(".link").on("mouseout", function(){
            $(this).css("background-color", "#fff");
        });

        // Pre-save
        $(".pre-save").click(function() { 
            $(".modal").fadeOut();
            $(".pre-save-modal").fadeIn();
            $("main").addClass('modal-open');
        });
        $(".btn-close").on("click", function(e){
            e.preventDefault();
            $(this).parents(".modal").fadeOut();
            $("main").removeClass('modal-open');
            return false;
        });
    });
})(jQuery, this);