var canvasHoz = document.getElementById("card-printer-hoz"),
    ctxHoz = canvasHoz.getContext("2d");

var canvasVrt = document.getElementById("card-printer-vrt"),
    ctxVrt = canvasVrt.getContext("2d");

var canvasSqr = document.getElementById("card-printer-sqr"),
    ctxSqr = canvasSqr.getContext("2d");

var cardPreview = document.getElementById("card-preview");

var mobilePreviewHoz = document.getElementById("mobile-card-preview-hoz");
var mobilePreviewSqr = document.getElementById("mobile-card-preview-sqr");
var mobilePreviewVrt = document.getElementById("mobile-card-preview-vrt");

var backgroundHoz = new Image();
var backgroundVrt = new Image();
var backgroundSqr = new Image();

backgroundHoz.src = "./../img/horizontal/ROC_01_16x9.jpg";
backgroundVrt.src = "./../img/vertical/ROC_01_9x16.jpg";
backgroundSqr.src = "./../img/square/ROC_01_1x1.jpg";

var textColorHoz = 'black';
var textColorVrt = 'white';
var textColorSqr = 'black';

var toNameAlignVrt = 'left';
var fromNameAlignVrt = 'center';

var toNamePosVrt = [50, 320];
var fromNamePosVrt = [canvasVrt.width/2, 1640];

var fontSizeVrt = 56;

(function ($, root, undefined) {
	$(function () {
		'use strict';

        $(".print-card").on("click", function(){
            createCards();
            $("#create-section").fadeOut();
            $("#share-section").fadeIn();
        });

        $("#edit-card").on("click", function(){
            $("#share-section").fadeOut();
            $("#create-section").fadeIn();
        });

        $(".heart").on("click", function(e){
            var option = $(this).data("option");
            $(".heart").removeClass("selected");            
            $(this).addClass("selected");

            backgroundHoz.src = "./../img/horizontal/ROC_"+option+"_16x9.jpg";
            backgroundVrt.src = "./../img/vertical/ROC_"+option+"_9x16.jpg";
            backgroundSqr.src = "./../img/square/ROC_"+option+"_1x1.jpg";

            switch(option){
                case "01":
                    textColorHoz = 'black';
                    textColorVrt = 'black';
                    textColorSqr = 'black';
                    toNameAlignVrt = 'center';
                    fromNameAlignVrt = 'center';
                    toNamePosVrt = [canvasVrt.width/2, 320];
                    fromNamePosVrt = [canvasVrt.width/2, 1640];
                    fontSizeVrt = 56;
                    break;
                case "02":
                    textColorHoz = 'white';
                    textColorVrt = 'white';                    
                    textColorSqr = 'white';
                    toNameAlignVrt = 'left';
                    fromNameAlignVrt = 'left';
                    toNamePosVrt = [50, 320];
                    fromNamePosVrt = [50, 1640];                    
                    fontSizeVrt = 56;
                    break;
                case "03":
                    textColorHoz = 'black';
                    textColorVrt = 'black';
                    textColorSqr = 'black';
                    toNameAlignVrt = 'center';
                    fromNameAlignVrt = 'center';
                    toNamePosVrt = [canvasVrt.width/2, 280];
                    fromNamePosVrt = [canvasVrt.width/2, 1670];
                    fontSizeVrt = 64;
                    break;
            }

            backgroundHoz.onload = function(){
                drawImageScaled(backgroundHoz, ctxHoz);  
            }
            backgroundVrt.onload = function(){
                drawImageScaled(backgroundVrt, ctxVrt);  
            }
            backgroundSqr.onload = function(){
                drawImageScaled(backgroundSqr, ctxSqr);  
            }
        });

        window.addEventListener('resize', function(event){
            var newWidth = window.innerWidth;
            
            displayMobile(newWidth);
            
        });

        displayMobile(window.innerWidth);

    });
})(jQuery, this);

var cardImages = [];

// Make sure the image is loaded first otherwise nothing will draw.
backgroundHoz.onload = function(){
    drawImageScaled(backgroundHoz, ctxHoz);  
}
backgroundVrt.onload = function(){
    drawImageScaled(backgroundVrt, ctxVrt);  
}
backgroundSqr.onload = function(){
    drawImageScaled(backgroundSqr, ctxSqr);  
}

function drawImageScaled(img, the_ctx) {
    var canvas = the_ctx.canvas ;
    var hRatio = canvas.width  / img.width    ;
    var vRatio =  canvas.height / img.height  ;
    var ratio  = Math.min ( hRatio, vRatio );
    var centerShift_x = ( canvas.width - img.width*ratio ) / 2;
    var centerShift_y = ( canvas.height - img.height*ratio ) / 2;  
    the_ctx.clearRect(0,0,canvas.width, canvas.height);
    the_ctx.drawImage(img, 0,0, img.width, img.height,
                       centerShift_x,centerShift_y,img.width*ratio, img.height*ratio); 
    console.log("final width: " + img.width*ratio + " | "); 
    console.log("final height: " + img.height*ratio);
 }

 function drawLongName(the_ctx, text, fontSize, pos, textColor, align) {
     the_text = "Love " + text;
     the_ctx.fillStyle = textColor;
     the_ctx.textAlign = align;
     the_ctx.font = fontSize+'px NeueHelvetica85Heavy';
     the_ctx.fillText(the_text.toUpperCase(), pos[0], pos[1]);
  }

  function drawToName(the_ctx, text, fontSize, pos, textColor, align) {
    the_text = "To " + text;
    the_ctx.fillStyle = textColor;
    the_ctx.textAlign = align;
    the_ctx.font = fontSize+'px NeueHelvetica85Heavy';
    the_ctx.fillText(the_text.toUpperCase(), pos[0], pos[1]);
  }

  function drawMessage(the_ctx, text, fontSize, pos, boxWidth, lineHeight, textColor) {
    the_text = text;
    the_ctx.fillStyle = textColor;
    the_ctx.font = fontSize+'px NeueHelvetica85Heavy';
    //the_ctx.fillText(the_text.toUpperCase(), 1080, 600);
    wrapText(the_ctx, the_text.toUpperCase(), pos[0], pos[1], boxWidth, lineHeight);
  }
  

 var download = function(format){
    var link = document.createElement('a');
     if(format == "hoz"){
        link.download = 'valentine_card_h.png';
        link.href = canvasHoz.toDataURL();
     }
     if(format == "sqr"){
        link.download = 'valentine_card_s.png';
        link.href = canvasSqr.toDataURL();
     }
     if(format == "vrt"){
        link.download = 'valentine_card_v.png';
        link.href = canvasVrt.toDataURL();
     }    
    link.click();
    link.remove();
  }

  var downloadZip = function(){  

    imageHoz = document.createElement('img');
    imageHoz.src = canvasHoz.toDataURL();

    imageVrt = document.createElement('img');
    imageVrt.src = canvasVrt.toDataURL();

    imageSqr = document.createElement('img');
    imageSqr.src = canvasSqr.toDataURL();

    var img1 = imageHoz.src.replace(/^data:image\/(png|jpg);base64,/, "");
    var img2 = imageVrt.src.replace(/^data:image\/(png|jpg);base64,/, "");
    var img3 = imageSqr.src.replace(/^data:image\/(png|jpg);base64,/, "");
    
    var zip = new JSZip();
        zip.folder("images");
        var img = zip.folder("images");
        img.file("ROC_16x9.png", img1, {base64: true});
        img.file("ROC_9x16.png", img2, {base64: true});
        img.file("ROC_1x1.png", img3, {base64: true});
        zip.generateAsync({type:"blob"}).then(function(content) {
            saveAs(content, "ROC_Valentines.zip");
        });  
  }

  function wrapText(context, text, x, y, maxWidth, lineHeight) {
    var words = text.split(' ');
    var line = '';

    for(var n = 0; n < words.length; n++) {
      var testLine = line + words[n] + ' ';
      var metrics = context.measureText(testLine);
      var testWidth = metrics.width;
      if (testWidth > maxWidth && n > 0) {
        context.fillText(line, x, y);
        line = words[n] + ' ';
        y += lineHeight;
      }
      else {
        line = testLine;
      }
    }
    context.fillText(line, x, y);
  }

  function createCards(){
    drawImageScaled(backgroundHoz, ctxHoz);
    drawImageScaled(backgroundVrt, ctxVrt);
    drawImageScaled(backgroundSqr, ctxSqr);    
    var longName = document.getElementById("longname");
    var toName = document.getElementById("receiver");
    var message = document.getElementById("message");

    drawLongName(ctxHoz, longName.value, 56, [1080, 835], textColorHoz, "left");
    drawToName(ctxHoz, toName.value, 56, [1080, 485], textColorHoz, "left");
    drawMessage(ctxHoz, message.value, 56, [1080, 600], 700, 115, textColorHoz);

    drawLongName(ctxVrt, longName.value, fontSizeVrt, fromNamePosVrt, textColorVrt, fromNameAlignVrt);
    drawToName(ctxVrt, toName.value, fontSizeVrt, toNamePosVrt, textColorVrt, toNameAlignVrt);

    drawLongName(ctxSqr, longName.value, 30, [600, 690], textColorSqr, "left");
    drawToName(ctxSqr, toName.value, 30, [600, 510], textColorSqr, "left");
    drawMessage(ctxSqr, message.value, 30, [600, 570], 400, 60, textColorSqr);

    cardPreview.src = canvasHoz.toDataURL();
    mobilePreviewHoz.src = cardPreview.src;
    mobilePreviewSqr.src = canvasSqr.toDataURL();
    mobilePreviewVrt.src = canvasVrt.toDataURL();
  }

  function posTest(){
    drawImageScaled(backgroundHoz, ctxHoz);
    drawImageScaled(backgroundVrt, ctxVrt);
    drawImageScaled(backgroundSqr, ctxSqr);
    var longName = document.getElementById("longname");
    var toName = document.getElementById("receiver");
    var message = document.getElementById("message");

    textColor = 'black';
    drawLongName(ctxHoz, longName.value, 56, [1080, 835]);
    drawToName(ctxHoz, toName.value, 56, [1080, 485]);
    drawMessage(ctxVrt, message.value, 56, [1080, 600], 700, 115);

    textColor = 'white';
    drawLongName(ctxVrt, longName.value, 56, [50, 1640]);
    drawToName(ctxVrt, toName.value, 56, [50, 320]);

    textColor = 'black';
    drawLongName(ctxSqr, longName.value, 30, [600, 690]);
    drawToName(ctxSqr, toName.value, 30, [600, 510]);
    drawMessage(ctxSqr, message.value, 30, [600, 570], 400, 60);
  }

  function displayMobile(currentWidth){
    if(currentWidth < 760){
        $("#card-preview").hide();
        $("#print-card").hide();
        $("#mobile-card-display").show();
    } else {
        $("#card-preview").show();
        $("#print-card").show();
        $("#mobile-card-display").hide();
    }
  }